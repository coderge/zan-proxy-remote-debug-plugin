# zan-proxy-remote-debug-plugin

[zan-proxy](https://github.com/youzan/zan-proxy) 插件, 功能包括:

* 向HTML页面注入 `vconsole` 工具

例如原本的页面内容为:
```html
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

开启注入vconsole功能后的页面内容为:
```html
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <script src="https://cdn.bootcss.com/vConsole/3.2.0/vconsole.min.js"></script><script>new VConsole();</script>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

`vconsole` 介绍 [https://github.com/Tencent/vConsole](https://github.com/Tencent/vConsole)

## 修改插件配置

在 zan-proxy 的插件管理页面中, 点击 zan-proxy-remote-debug 插件即可进入插件配置页面

## 使用 vconsole

开启 `注入vconsole` 功能后, 再次打开页面, 页面右下角将会多出一个绿色的vConsole悬浮按钮, 如下图:

![](https://t1.aixinxi.net/o_1ckegjhs611al1en7l2v109f262a.png-j.jpg)
