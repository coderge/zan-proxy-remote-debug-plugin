const Koa = require('koa')
const Static = require('koa-static')
const Router = require('koa-router')
const BodyParser = require('koa-bodyparser')
const path = require('path')
const { byteLength } = require('byte-length')
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const zlib = require('zlib')

// 配置文件存放路径
const configFilePath = path.resolve(__dirname, './config.json')
const adapter = new FileSync(configFilePath)

module.exports = class RemoteDebugPlugin {
  constructor() {
    const db = this.db = low(adapter)
    db.defaults({
      vConsole: false,
      eruda: false
    }).write()
    this.config = {
      vConsole: db.get('vConsole').value(),
      eruda: db.get('eruda').value()
    }
  }

  proxy() {
    return async (ctx, next) => {
      await next()

      const contentType = ctx.res.getHeader('content-type') || ''
      const contentEncoding = ctx.res.getHeader('content-encoding')
      // 只拦截html文件
      const shouldInspect = contentType.indexOf('text/html') > -1
      const needInject = Object.values(this.config).some(enable => enable)
      if(shouldInspect && needInject) {
        const getBodyPromise = new Promise(resolve => {
          let body = Buffer.allocUnsafe(0)
          ctx.res.body.on('data', buffer => {
            body = Buffer.concat([body, buffer]);
          })
          ctx.res.body.on('end', () => {
            resolve(body)
          })
        })
        const contentStream = await getBodyPromise
        // 如果是gzip的话，需要解压
        if(contentEncoding === 'gzip') {
          contentStream = zlib.gunzipSync(contentStream)
          ctx.res.removeHeader('content-encoding')
        }
        const contentString = contentStream.toString()
        // 找到插入点（head闭合标签前、第一个script标签前，取最靠前的一个）
        const headEndIndex = contentString.indexOf('</head>')
        const firstScriptIndex = contentString.indexOf('<script>')
        const injectIndex = Math.min(...[headEndIndex, firstScriptIndex].filter(i => i > -1))
        if(injectIndex > -1) {
          const injectBefore = contentString.slice(0, injectIndex)
          const injectAfter = contentString.slice(injectIndex, contentString.length)
          const injectScripts = []
          // 插入所需要的脚本代码
          if(this.config.vConsole) {
            injectScripts.push('<script src="https://unpkg.com/vconsole"></script><script>new VConsole();</script>')
          }
          if(this.config.eruda) {
            injectScripts.push('<script src="https://unpkg.com/eruda"></script><script>eruda.init();</script>')
          }
          const result = injectBefore + injectScripts.join('') + injectAfter
          ctx.res.setHeader('content-length', byteLength(result))
          ctx.res.body = result
        }
      }
    }
  }

  manage() {
    // manage方法需要返回一个Koa实例
    const app = new Koa()
    // 我们将静态资源放置在static目录下
    app.use(Static(path.resolve(__dirname, './static')))
    app.use(BodyParser())
    // 通过router接收请求，修改配置
    const router = new Router()
    // 列出配置详情
    router.get('/config', async (ctx) => {
      ctx.body = this.config
    })
    // 修改vConsole启用状态
    router.post('/vConsole', async (ctx) => {
      const enable = ctx.request.body.enable
      this.config.vConsole = enable
      this.db.set('vConsole', enable).write()
      ctx.body = this.config
    })
    // 修改eruda启用状态
    router.post('/eruda', async (ctx) => {
      const enable = ctx.request.body.enable
      this.config.eruda = enable
      this.db.set('eruda', enable).write()
      ctx.body = this.config
    })
    app.use(router.routes())
    app.use(router.allowedMethods())
    return app
  }
}
